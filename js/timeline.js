$(function () {

var uid = 0;
$('#timeline_add').click(function (event) {
    event.preventDefault();
    var item = $('#timeline_tmpl').html();
    uid++;
    item = item.replace(/\{\{uid\}\}/g, uid);
    var $field = $(item).find('input[type=file]');
    $('#timeline').append(item);
    initUpload($field);
});

$('#timeline').on('click', 'a.timeline_remove', function (event) {
    event.preventDefault();
    $(this).closest('.timeline_item').fadeOut(function () {
        $(this).remove();
    });
});

$('#timeline').on('click', 'a.timeline_prev', function (event) {
    event.preventDefault();
    var $item = $(this).closest('.timeline_item');
    $item.insertBefore($item.prev());
});

$('#timeline').on('click', 'a.timeline_next', function (event) {
    event.preventDefault();
    var $item = $(this).closest('.timeline_item');
    $item.insertAfter($item.next());
});

function initUpload(field, uid) {
    $(field).uploadify({
        buttonText:'上传附件',
        auto:false,
        progressData:'speed',
        multi:true,//false上传一个文件，true上传多个文件
        height:25,
        overrideEvents:['onDialogClose'],
        fileTypeDesc:'文件格式:',
        fileTypeExts:'*.*',//文件格式类型
        queueID: $(field).parent().find('.filediv_attachment').attr('id'),
        fileSizeLimit:'15MB',
        swf:'http://59.110.231.159:8099/jeecg/plug-in/uploadify/uploadify.swf',  
        uploader:'cgUploadController.do?saveFiles&jsessionid='+$("#sessionUID").val()+'',
        onUploadStart : function(file) { 
            var cgFormId=$("input[name='id']").val();
            $('#attachment').uploadify("settings", "formData", {
                'cgFormId':cgFormId,
                'cgFormName':'t_b_news',
                'cgFormField':'attachment'
            });
        } ,
        onQueueComplete : function(queueData) {
             var win = frameElement.api.opener;
             win.reloadTable();
             win.tip(serverMsg);
             frameElement.api.close();
        },
        onUploadSuccess : function(file, data, response) {
            var d=$.parseJSON(data);
            if(d.success){
                var win = frameElement.api.opener;
                serverMsg = d.msg;
            }
        },
        onFallback: function() {
            tip("您未安装FLASH控件，无法上传图片！请安装FLASH控件后再试")
        },
        onSelectError: function(file, errorCode, errorMsg) {
            switch (errorCode) {
            case - 100 : tip("上传的文件数量已经超出系统限制的数量！");
                break;
            case - 110 : tip("文件 [" + file.name + "] 大小超出系统限制的" + $('#file').uploadify('settings', 'fileSizeLimit') + "大小！");
                break;
            case - 120 : tip("文件 [" + file.name + "] 大小异常！");
                break;
            case - 130 : tip("文件 [" + file.name + "] 类型不正确！");
                break;
            }
        },
        onUploadProgress: function(file, bytesUploaded, bytesTotal, totalBytesUploaded, totalBytesTotal) {}
    });
}

function form() {
    $('#timeline_submit').click(function (event) {
        $('#timeline_form').submit();
    });

    $('#timeline_form').submit(function (event) {
        event.preventDefault();
        var events = [];
        $(this).find('.timeline_item').each(function () {
            var src = $(this).find('img').attr('src'); // TODO 请找到图片地址
            var content = $(this).find('textarea').val();
            events.push({
                cover: src,
                content: content
            });
        });
        var params = {
            year: $('#year').val(),
            events: events
        };

        // TODO 请发送POST请求
        console.info('TODO 请发送POST请求，参数=', params);
    });
}

function start() {
    $('#timeline .timeline_item').each(function () {
        var field = $(this).find('input.attachment');
        initUpload(field);
    });
    form();
}

start();

});